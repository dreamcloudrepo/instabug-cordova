const fs = require('fs'),
    path = require('path');

module.exports = function (context) {
    const toolsAttribute = "xmlns:tools=\"http://schemas.android.com/tools\"";
    const manifestOpen = "<manifest";
    const androidMaxSDK = "<uses-permission android:name=\"android.permission.WRITE_EXTERNAL_STORAGE\" />";

    const manifestPath = path.join(context.opts.projectRoot, 'platforms/android/app/src/main/AndroidManifest.xml');
    let manifest = fs.readFileSync(manifestPath).toString();
    let isWrite = false;
    console.log(manifest);
    if(manifest.indexOf(toolsAttribute) == -1) {
        console.log('Editing toolsAttribute');
        manifest = manifest.replace(manifestOpen, manifestOpen + " " + toolsAttribute + " ");
        isWrite = true;
    }
    
    if(manifest.indexOf(androidMaxSDK) > -1) {
        console.log('Removing permission maxSdkVersion from AndroidManifest.xml');
        manifest = manifest.replace(androidMaxSDK, "");
        isWrite = true;
    }
    
    if(isWrite) {
        console.log('Writing');
        fs.writeFileSync(manifestPath, manifest, 'utf8');
    }
    
};

/*

const fs = require('fs/promises')
const xml2js = require('xml2js')

const REMOVE_PERMISSIONS = [
  'android.permission.WRITE_EXTERNAL_STORAGE'
]

module.exports = async function(context) {
  const root = context.opts.projectRoot
  const manifestPath = root + '/platforms/android/app/src/main/AndroidManifest.xml'
  const manifestXml = await fs.readFile(manifestPath)
  const manifest = await xml2js.parseStringPromise(manifestXml)
  const usesPermissions = manifest.manifest['uses-permission']
  if (Array.isArray(usesPermissions)) {
    manifest.manifest['uses-permission'] = usesPermissions.filter(usesPermission => {
      const attrs = usesPermission.$ || {}
      const name = attrs['android:name'] // Assuming xmlns:android has been set as usual...
      if (REMOVE_PERMISSIONS.includes(name)) {
        console.log(`Removing permission "${name}" from AndroidManifest.xml`)
        return false
      } else {
        return true
      }
    })
  }
  const newManifest = (new xml2js.Builder()).buildObject(manifest)
  await fs.writeFile(manifestPath, newManifest)
}
*/