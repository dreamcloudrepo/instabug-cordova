var Instabug = cordova.require("instabug-cordova.Instabug");
var BugReporting = cordova.require("instabug-cordova.BugReporting");
var FeatureRequests = cordova.require("instabug-cordova.FeatureRequests");
var Surveys = cordova.require("instabug-cordova.Surveys");
var Replies = cordova.require("instabug-cordova.Replies");

module.exports = {
  Instabug,
  BugReporting,
  FeatureRequests,
  Surveys,
  Replies,
};
